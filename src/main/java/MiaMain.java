import com.mashape.unirest.http.exceptions.UnirestException;
import kz.ayt.mia.GenreList;
import kz.ayt.mia.MiaBot;
import kz.ayt.mia.PagesNum;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.exceptions.TelegramApiException;

/**
 * Created by a123 on 25.10.17.
 */

@SpringBootApplication
public class MiaMain {
    public static void main(String[] args) {

        try {
            GenreList.Instance().addGenre("en");
            GenreList.Instance().addGenre("ru");
            PagesNum.Instance().getAllTotalPages();
        } catch (Exception e) {
            e.printStackTrace();
        }

        ApiContextInitializer.init();
        TelegramBotsApi botsApi = new TelegramBotsApi();
        try {
            botsApi.registerBot(new MiaBot());
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }

    }
}

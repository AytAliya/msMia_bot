package kz.ayt.mia.model;

/**
 * Created by a123 on 20.11.17.
 */
public class MoviePage {
    private int year;
    private int genre;
    private int page;

    public int getYear() {
        return year;
    }

    public int getGenre() {
        return genre;
    }

    public int getPage() {
        return page;
    }

    public MoviePage(int year, int genre, int page) {
        this.year = year;
        this.genre = genre;
        this.page = page;
    }
}

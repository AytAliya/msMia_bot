package kz.ayt.mia;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import kz.ayt.mia.model.MoviePage;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.api.methods.ActionType;
import org.telegram.telegrambots.api.methods.send.SendChatAction;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.send.SendPhoto;
import org.telegram.telegrambots.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardRemove;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import org.slf4j.Logger;

/**
 * Created by a123 on 25.10.17.
 */
public class MiaBot extends TelegramLongPollingBot {

    Logger log = LoggerFactory.getLogger(MiaBot.class);

    String language = "en";
    int releaseYear = 2017;
    int genreId = 35;
    ArrayList<String> yearsArr = new ArrayList<String>(Arrays.asList("/2017", "/2016", "/2015",
            "/2014", "/2013", "/2012", "/2011", "/2010", "/2009", "/2008", "/2007", "/2006", "/2005",
            "/2004", "/2003", "/2002", "/2001", "/2000", "/1999", "/1998"));
    ArrayList<String> genresListEn = GenreList.Instance().getAllGenresList("en");
    ArrayList<String> genresListRu = GenreList.Instance().getAllGenresList("ru");

    class Movie {
        private String title;
        private String overview;
        private String releasedDate;
        private String genre;

        public String getImagePath() {
            return imagePath;
        }

        public void setImagePath(String imagePath) {
            this.imagePath = imagePath;
        }

        private String imagePath;

        public Movie(String title, String overview, String releasedDate, String genre, String imagePath) {
            this.title = title;
            this.overview = overview;
            this.releasedDate = releasedDate;
            this.genre = genre;
            this.imagePath = imagePath;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getOverview() {
            return overview;
        }

        public void setOverview(String overview) {
            this.overview = overview;
        }

        public String getReleasedDate() {
            return releasedDate;
        }

        public void setReleasedDate(String releasedDate) {
            this.releasedDate = releasedDate;
        }

        public String getGenre() {
            return genre;
        }

        public void setGenre(String genre) {
            this.genre = genre;
        }
    }

    public void onUpdateReceived(Update update) {
        Message message = update.getMessage();

        if (message!=null && update.getMessage().hasText()) {

            log.info(message.toString());

            if(message.getText().equals("/start")){
                sendMsg(message,"Hey, my name is Mia \uD83D\uDE4B\u200D♀️ I can choose you a movie to watch \uD83C\uDFA5 Call me by /movie");
            } else if(message.getText().equals("/language")){
                addLangKeyboard(message);
            } else if(message.getText().equals("/English")){
                language = "en";
                removeKeyboard(message, "English language", "русский язык");
            } else if(message.getText().equals("/Русский")){
                language = "ru";
                removeKeyboard(message, "English language", "русский язык");
            }else if(message.getText().equals("/year")){
                chooseYear(message);
            }else if(message.getText().equals("/genre")){
                chooseGenre(message);
            }else if(message.getText().equals("/movie")){
                String sendText = language == "en" ? "Cool! Let me choose you a movie \uD83C\uDFA5" : "Круто! Давайте, подберем вам фильм \uD83C\uDFA5";
                sendMsg(message, sendText);
                sendAction(message);
                Movie movie = getMovie();
                while(movie.getOverview().equals("") || movie.getReleasedDate().equals("")
                        || movie.getGenre().equals("") || movie.getImagePath().equals("")){
                    movie = getMovie();
                }
                String text = prepareMovieText(movie);
                sendImg(message, movie.getImagePath(), movie.getTitle());
                sendMovieMsg(message, text);
            }else if(yearsArr.contains(message.getText())){
                releaseYear = Integer.parseInt(message.getText().substring(1,5));
                removeKeyboard(message, "Release year " + releaseYear, "год выпуска: " + releaseYear);
            }else if(genresListEn.contains(message.getText()) || genresListRu.contains(message.getText())){
                String genreStr = message.getText().substring(1,message.getText().length());
                genreId = GenreList.Instance().getGenreId(language, genreStr);
                removeKeyboard(message, "Genre " + genreStr, "жанр: " + genreStr);
            }else if(message.getText().substring(0, 1).equals("/")){
                String sendText = language == "en" ? "Sorry, i don't know such command \uD83E\uDD37" : "Извините, я незнаю такой команды \uD83E\uDD37";
                sendMsg(message, sendText);
            }
        } else if (update.hasCallbackQuery()) {
            String call_data = update.getCallbackQuery().getData();
            if (call_data.equals("update_next_film")) {
                Movie movie = getMovie();
                while(movie.getOverview().equals("") || movie.getReleasedDate().equals("") || movie.getGenre().equals("")){
                    movie = getMovie();
                }
                String text = prepareMovieText(movie);
                sendImg(update.getCallbackQuery().getMessage(), movie.getImagePath(), movie.getTitle());
                sendMovieMsg(update.getCallbackQuery().getMessage(), text);
            }
        }

    }

    private Movie fetchRandomMovie() throws Exception {
        int totalPages = PagesNum.Instance().getTotalPages(releaseYear, genreId);//getPagesNum();
        int randomPage = (int) (Math.random() * totalPages);
        randomPage = randomPage == 0 ? 1 : randomPage;
        System.out.println("randomPage: " + randomPage);
        HttpResponse<JsonNode> response = Unirest.get("https://api.themoviedb.org/3/discover/movie")
                .queryString("api_key", "bbe36a7618b67875d1bbcc7e3f242469")
                .queryString("language", language)
                .queryString("sort_by", "popularity.desc")
                .queryString("include_adult", "false")
                .queryString("primary_release_year", releaseYear)
                .queryString("with_genres", genreId)
                .queryString("page", randomPage)
                .asJson();
        JSONArray results = response.getBody().getObject().getJSONArray("results");
        int randomIndex = (int) Math.random() * (results.length() - 1);
        JSONObject randomMovie = results.getJSONObject(randomIndex);
        String title = randomMovie.getString("title");
        String overview = randomMovie.getString("overview");
        String releaseDate = randomMovie.getString("release_date");
        String imagePath = randomMovie.optString("poster_path", "");
        JSONArray genrIds = randomMovie.getJSONArray("genre_ids");
        ArrayList<Integer> genresArray = new ArrayList<Integer>();
        for(int i=0; i<genrIds.length(); i++){
            genresArray.add((Integer) genrIds.get(i));
        }
        String genres = GenreList.Instance().getGenre(genresArray, language);
        String imgPathToSend = imagePath==""?"":"http://image.tmdb.org/t/p/original"+imagePath;
        return new Movie(title, overview, releaseDate, genres, imgPathToSend);
    }

    private Movie getMovie(){
        Movie movie = null;
        try {
            movie = fetchRandomMovie();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return movie;
    }

    private String prepareMovieText(Movie movie){
        String text = language.equals("en") ? "\uD83D\uDD38 *Title*: " + movie.getTitle() + "\n"
                + "\uD83D\uDD39 *Overview* : " + movie.getOverview() + "\n"
                + "\uD83D\uDD38 *Release date* : " + movie.getReleasedDate() + "\n"
                + "\uD83D\uDD39 *Genre* : " + movie.getGenre() + "\n"
                : "\uD83D\uDD38 *Название*: " + movie.getTitle() + "\n"
                + "\uD83D\uDD39 *Описание* : " + movie.getOverview() + "\n"
                + "\uD83D\uDD38 *Дата выхода* : " + movie.getReleasedDate() + "\n"
                + "\uD83D\uDD39 *Жанр* : " + movie.getGenre() + "\n";
        return text;
    }

    private void sendAction(Message message){
        SendChatAction action = new SendChatAction();
        action.setChatId(message.getChatId());
        action.setAction(ActionType.TYPING);
        try {
            execute(action);
        } catch (TelegramApiException e) {
            log.error("sendAction>>Error in sending message to client " + e);
        }
    }

    private void sendImg(Message message, String imgPath, String text){
        SendPhoto sendPhoto = new SendPhoto();
        sendPhoto.setChatId(message.getChatId().toString());
        sendPhoto.setPhoto(imgPath);
        sendPhoto.setCaption(text);
        try {
            sendPhoto(sendPhoto);
        } catch (TelegramApiException e) {
            log.error("sendImg>>Error in sending message to client " + e);
        }
    }

    private void sendMovieMsg(Message message, String text){
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(message.getChatId().toString());
        sendMessage.setText(text);
        InlineKeyboardMarkup markupLine = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInLine = new ArrayList<List<InlineKeyboardButton>>();
        List<InlineKeyboardButton> rowInLine = new ArrayList<InlineKeyboardButton>();
        rowInLine.add(new InlineKeyboardButton().setText("Next").setCallbackData("update_next_film"));
        InlineKeyboardButton btn = new InlineKeyboardButton();
        rowsInLine.add(rowInLine);
        markupLine.setKeyboard(rowsInLine);
        sendMessage.setReplyMarkup(markupLine);
        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            log.error("sendMovieMsg>>Error in sending message to client " + e);
        }
    }

    private void sendMsg(Message message, String text){
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(message.getChatId().toString());
        sendMessage.setText(text);
        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            log.error("sendMovieMsg>>Error in sending message to client " + e);
        }
    }

    private void addLangKeyboard(Message message){
        SendMessage sendMessage = new SendMessage()
                .setChatId(message.getChatId())
                .setText(language=="en"?"✨Here are your options:":"✨Вот ваши варианты:");
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        List<KeyboardRow> keyboard = new ArrayList<KeyboardRow>();
        KeyboardRow row = new KeyboardRow();
        row.add("/English");
        row.add("/Русский");
        keyboard.add(row);
        keyboardMarkup.setOneTimeKeyboard(true);
        keyboardMarkup.setKeyboard(keyboard);
        sendMessage.setReplyMarkup(keyboardMarkup);
        try {
            execute(sendMessage); // Sending our message object to user
        } catch (TelegramApiException e) {
            log.error("sendMovieMsg>>Error in sending message to client " + e);
        }
    }

    private void chooseYear(Message message){
        SendMessage sendMessage = new SendMessage()
                .setChatId(message.getChatId())
                .setText(language=="en"?"✨Here are your options:":"✨Вот ваши варианты:");
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        List<KeyboardRow> keyboard = new ArrayList<KeyboardRow>();
        KeyboardRow row1 = new KeyboardRow();
        row1.add("/2017");
        row1.add("/2016");
        row1.add("/2015");
        row1.add("/2014");
        keyboard.add(row1);
        KeyboardRow row2 = new KeyboardRow();
        row2.add("/2013");
        row2.add("/2012");
        row2.add("/2011");
        row2.add("/2010");
        keyboard.add(row2);
        KeyboardRow row3 = new KeyboardRow();
        row3.add("/2009");
        row3.add("/2008");
        row3.add("/2007");
        row3.add("/2006");
        keyboard.add(row3);
        KeyboardRow row4 = new KeyboardRow();
        row4.add("/2005");
        row4.add("/2004");
        row4.add("/2003");
        row4.add("/2002");
        keyboard.add(row4);
        KeyboardRow row5 = new KeyboardRow();
        row5.add("/2001");
        row5.add("/2000");
        row5.add("/1999");
        row5.add("/1998");
        keyboard.add(row5);
        keyboardMarkup.setOneTimeKeyboard(true);
        keyboardMarkup.setKeyboard(keyboard);
        sendMessage.setReplyMarkup(keyboardMarkup);
        try {
            execute(sendMessage); // Sending our message object to user
        } catch (TelegramApiException e) {
            log.error("sendMovieMsg>>Error in sending message to client " + e);
        }
    }

    private void chooseGenre(Message message){
        SendMessage sendMessage = new SendMessage()
                .setChatId(message.getChatId())
                .setText(language=="en"?"✨Here are your options:":"✨Вот ваши варианты:");
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        List<KeyboardRow> keyboard = new ArrayList<KeyboardRow>();
        KeyboardRow row1 = new KeyboardRow();
        row1.add(language=="en"?"/Comedy":"/комедия");
        row1.add(language=="en"?"/Action":"/боевик");
        row1.add(language=="en"?"/Animation":"/мультфильм");
        keyboard.add(row1);
        KeyboardRow row2 = new KeyboardRow();
        row2.add(language=="en"?"/Drama":"/драма");
        row2.add(language=="en"?"/Horror":"/ужасы");
        row2.add(language=="en"?"/Adventure":"/приключения");
        keyboard.add(row2);
        KeyboardRow row3 = new KeyboardRow();
        row3.add(language=="en"?"/Fantasy":"/фэнтези");
        row3.add(language=="en"?"/Thriller":"/триллер");
        row3.add(language=="en"?"/Mystery":"/детектив");
        keyboard.add(row3);
        keyboardMarkup.setOneTimeKeyboard(true);
        keyboardMarkup.setKeyboard(keyboard);
        sendMessage.setReplyMarkup(keyboardMarkup);
        try {
            execute(sendMessage); // Sending our message object to user
        } catch (TelegramApiException e) {
            log.error("sendMovieMsg>>Error in sending message to client " + e);
        }
    }

    private void removeKeyboard(Message message, String textEn, String textRu){
        SendMessage sendMessage = new SendMessage()
                .setChatId(message.getChatId())
                .setText(language == "en"? "✔️" + textEn + " is chosen  \n Movie? /movie" : "✔️ Был выбран " + textRu + " \n Фильм? /movie");
        ReplyKeyboardRemove replyKeyboardRemove = new ReplyKeyboardRemove();
        sendMessage.setReplyMarkup(replyKeyboardRemove);
        try {
            execute(sendMessage); // Sending our message object to user
        } catch (TelegramApiException e) {
            log.error("sendMovieMsg>>Error in sending message to client " + e);
        }
    }

    public String getBotUsername() {
        return "msMia_bot";

    }

    public String getBotToken() {
        return ""; // bot token
    }
}

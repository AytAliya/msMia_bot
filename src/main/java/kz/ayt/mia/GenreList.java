package kz.ayt.mia;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by a123 on 03.11.17.
 */
public class GenreList {

    Logger log = LoggerFactory.getLogger(GenreList.class);

    private static class Singleton {
        private static final GenreList Instance = new GenreList();
    }

    public static GenreList Instance() {
        return Singleton.Instance;
    }

    private HashMap<Integer, String> genresMapEn = new HashMap<Integer, String>();
    private HashMap<Integer, String> genresMapRu = new HashMap<Integer, String>();

    public void addGenre(String language) throws Exception {
        HttpResponse<JsonNode> response = Unirest.get("https://api.themoviedb.org/3/genre/movie/list")
                .queryString("api_key", "bbe36a7618b67875d1bbcc7e3f242469")
                .queryString("language", language)
                .asJson();
        JSONArray results = response.getBody().getObject().getJSONArray("genres");
        for(int i=0; i<results.length(); i++){
            JSONObject genre = results.getJSONObject(i);
            if(language == "en"){
                genresMapEn.put(genre.getInt("id"), genre.getString("name"));
            } else {
                genresMapRu.put(genre.getInt("id"), genre.getString("name"));
            }
        }
        log.info("Done Adding genres for " + language);
    }

    public String getGenre(ArrayList<Integer> ids, String lang) {
        String genresText = "";
        for(int i =0; i<ids.size(); i++){
            if(lang=="en") {
                if (i == 0) genresText += genresMapEn.get(ids.get(i));
                else
                    genresText += ", " + genresMapEn.get(ids.get(i));
            } else{
                if (i == 0) genresText += genresMapRu.get(ids.get(i));
                else
                    genresText += ", " + genresMapRu.get(ids.get(i));
            }
        }
        return genresText;
    }

    /*public int[] getAllGenresId(){
        int[] genreIds = new int[genresMapEn.size()];
        int index = 0;
        for(Integer a: genresMapEn.keySet()){
            genreIds[index] = a;
            System.out.println("getAllGenresList>> " + a);
            index++;
        }
        System.out.println("getAllGenresList>>DONE>> " + genreIds.length);
       return genreIds;
    }*/

    public ArrayList<String> getAllGenresList(String lang){
        ArrayList<String> genresList = new ArrayList<String>();
        if(lang=="en"){
            for(String genre : genresMapEn.values()){
                genresList.add("/" + genre);
            }
        } else {
            for(String genre : genresMapRu.values()){
                genresList.add("/" + genre);
            }
        }
        return genresList;
    }

    public Integer getGenreId(String lang, String value){
        Integer idRet = 0;
        if(lang=="en"){
            for(Integer id : genresMapEn.keySet()){
                if(genresMapEn.get(id).equals(value)){
                    idRet = id;
                }
            }
        } else {
            for(Integer id : genresMapRu.keySet()){
                if(genresMapRu.get(id).equals(value)){
                    idRet = id;
                }
            }
        }
        return idRet;
    }
 }

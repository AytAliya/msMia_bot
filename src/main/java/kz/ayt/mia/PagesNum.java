package kz.ayt.mia;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import kz.ayt.mia.model.MoviePage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by a123 on 19.11.17.
 */
public class PagesNum {

    Logger log = LoggerFactory.getLogger(PagesNum.class);

    private static class Singleton {
        private static final PagesNum Instance = new PagesNum();
    }

    public static PagesNum Instance(){
        return Singleton.Instance;
    }

    private ArrayList<MoviePage> queryResult = new ArrayList<MoviePage>();
    private int[] yearsList = new int[]{2017, 2016, 2015, 2014, 2013, 2012, 2011, 2010, 2009, 2008, 2007, 2006, 2005, 2004, 2003, 2002, 2001, 2000, 1999, 1998};
    private int[] genreList = new int[]{35, 28, 16, 18, 27, 12, 14, 53, 9648};

    public void getAllTotalPages() throws Exception {
        System.out.println("@@ Getting all total pages @@");
        int count = 1;
        for(int year : yearsList){
            for(int genre : genreList){
                System.out.println("Count1: " + count);
                if(count%39==0){
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println("year: " + year + " genre: " + genre);
                HttpResponse<JsonNode> response = Unirest.get("https://api.themoviedb.org/3/discover/movie")
                        .queryString("api_key", "bbe36a7618b67875d1bbcc7e3f242469")
                        .queryString("include_adult", "false")
                        .queryString("primary_release_year", year)
                        .queryString("with_genres", genre)
                        .asJson();
                int totalPages = response.getBody().getObject().getInt("total_pages");
                count++;
                queryResult.add(new MoviePage(year, genre, totalPages));

            }
        }
        System.out.println("DONE getting all pages: " + queryResult.size());
        log.info("DONE getting all pages: " + queryResult.size());
    }

    public int getTotalPages(int year, int genre){
        int page = 1;
        for(MoviePage mv : queryResult){
            if(mv.getYear()==year && mv.getGenre()==genre){
                page = mv.getPage();
                break;
            }
        }
        return page;
    }
}
